from django.shortcuts import render, get_object_or_404
from collection.models import Resource, Collection


def resource(request, slug):
    obj = get_object_or_404(Resource, slug=slug)
    collection_set = obj.collection_set.all()
    content_all = obj.content_set.all().select_subclasses()
    data = {
        'obj': obj,
        'collection_set': collection_set,
        'content_all': content_all
    }
    return render(request, 'resource/resource.html', data)


def collection(request, slug, slug_c):
    res = get_object_or_404(Resource, slug=slug)
    col = get_object_or_404(Collection, slug=slug_c, resource=res)
    data = {
        'obj': res,
        'col': col,
    }
    return render(request, 'resource/collection.html', data)