from django.contrib import admin
from image_cropping import ImageCroppingMixin

from collection.models import Resource, Collection


class CollectionInline(admin.StackedInline):
    model = Collection
    extra = 0


class ResourceAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ['title', '__str__', 'slug']
    inlines = [CollectionInline,]
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Resource, ResourceAdmin)