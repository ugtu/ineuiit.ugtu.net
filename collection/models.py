import datetime
import os
from ckeditor.fields import RichTextField
from django.db import models
from django.urls import reverse
from laporem_field.forms import LaporemImageField
from laporem_field.models import LaporemImage
from pytils.translit import slugify
from autoslug import AutoSlugField
from ckeditor_uploader.fields import RichTextUploadingField

from core.models import Scale, Rating_Base


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    name = filename.split('.')[0:-1]
    filename = "%s.%s" % (slugify(name), ext)
    return os.path.join('collections', filename)



def get_name(obj, name):
    name = ' |-- ' + obj.title + name
    if obj.parent:
        return get_name(obj.parent, name)
    return name


class ImageResurse(LaporemImage):
    pass


class Resource(Rating_Base):
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name='Родительский ресурс')
    title = models.CharField(max_length=500, verbose_name="Заголовок", blank=True)
    slug = models.SlugField(max_length=255, unique=True)
    description = RichTextUploadingField(blank=True, verbose_name="Описание")
    slider = LaporemImageField(null=True, blank=True, laporem_model_file=ImageResurse, verbose_name="Изображения",
                               help_text="Галерея фото")

    def __str__(self):
        name = get_name(self, '')
        return name

    def get_absolute_url(self):
        return reverse('resource', args=[self.slug])

    def get_statistic(self):
        return self.collection_set.all()

    class Meta:
        verbose_name = 'Ресурс'
        verbose_name_plural = 'Ресурсы'
        ordering = ['title']


class Collection(models.Model):
    resource = models.ForeignKey(Resource)
    image = models.ImageField(upload_to=get_file_path, verbose_name="Фото", null=True, blank=True)
    file = models.FileField(upload_to=get_file_path, verbose_name="Файл", null=True, blank=True)
    title = models.CharField(max_length=500, verbose_name="Заголовок")
    description = RichTextUploadingField(blank=True, verbose_name="Описание")
    slug = AutoSlugField(populate_from='title', max_length=1000, unique_with=['id', ])
    date = models.DateTimeField(default=datetime.datetime.now, verbose_name="Дата")


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Объект ресурса'
        verbose_name_plural = 'Объекты ресурса'