from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from image_cropping import ImageCroppingMixin
from .models import User, SiteConfiguration, Image, Partner, Scale, Rating
from solo.admin import SingletonModelAdmin


class MyUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('middle_name',)}),
    )


class ImageAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass


class RatingAdmin(admin.ModelAdmin):
    list_display = ['object_id', 'result', 'date']


admin.site.register(User, MyUserAdmin)
admin.site.register(SiteConfiguration, SingletonModelAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Partner)
admin.site.register(Scale)
admin.site.register(Rating, RatingAdmin)


