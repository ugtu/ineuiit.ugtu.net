from django.apps import AppConfig


class YourAppConfig(AppConfig):
    name = 'core'
    verbose_name = 'Настройки сайта'
