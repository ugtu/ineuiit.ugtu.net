from ckeditor.fields import RichTextField
from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField
from pytils.translit import slugify
from django.utils.translation import ugettext_lazy as _
from laporem_field.forms import LaporemImageField
from laporem_field.models import LaporemImage
import os
from solo.models import SingletonModel
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import fields as generic


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    name = filename.split('.')[0:-1]
    filename = "%s.%s" % (slugify(name), ext)
    return os.path.join('setting', filename)


class User(AbstractUser):
    image = ThumbnailerImageField(upload_to=get_file_path, blank=True, verbose_name="Фото", null=True)
    cropping = ImageRatioField('image', '300x300', verbose_name="Обрезка превью")
    middle_name = models.CharField(
        _('Отчество'),
        max_length=50,
        null=True, blank=True,
        help_text=_('Required. 50 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Enter a valid username. This value may contain only '
                  'letters, numbers ' 'and @/./+/-/_ characters.')
            ),
        ],
    )

    def __str__(self):
        return self.username


class Image(LaporemImage):
    cropping = ImageRatioField('file', '1024x700', verbose_name="Обрезка")
    title = models.CharField(blank=True, max_length=500, verbose_name="Заголовок")
    description = RichTextField(blank=True, verbose_name="Описание")


class SiteConfiguration(SingletonModel):
    slider = LaporemImageField(null=True, blank=True, laporem_model_file=Image, help_text="Галерея фото")
    contacts = RichTextField(blank=True, verbose_name="Контакты")
    # description = RichTextField(blank=True, verbose_name="Описание")
    footer_info = models.TextField(blank=True, verbose_name="Описание в футере")

    class Meta:
        verbose_name = "Настройки сайта"


class Partner(models.Model):
    title = models.CharField(blank=True, max_length=500, verbose_name="Заголовок")
    image = models.ImageField(upload_to=get_file_path, verbose_name="Фото", null=True)
    url = models.CharField(blank=True, max_length=500)

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'

    def __str__(self):
        return self.title


class Scale(models.Model):
    title = models.CharField(max_length=500, verbose_name="Заголовок", blank=True)
    result = models.IntegerField(default=5, verbose_name='Шкала')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Шкала'
        verbose_name_plural = 'Шкалы'


class Rating(models.Model):
    content_type = models.ForeignKey(ContentType, editable=False)
    content_object = generic.GenericForeignKey()
    object_id = models.TextField(null=True, editable=False)
    result = models.IntegerField(default=0, verbose_name="Рейтинг")
    scale = models.ForeignKey(Scale)
    date = models.DateField(auto_now_add=True, null=True)

    class Meta:
        verbose_name = 'Рейтинг'
        verbose_name_plural = 'Рейтинги'


class Rating_Base(models.Model):
    scale = models.ForeignKey(Scale, null=True, blank=True, verbose_name='Рейтинг для объектов')

    def get_statistic(self):
        # Должен возвращать QuerySet тех объектов которые оцениваются
        return self.__class__.objects.filter(id=self.id)

    def get_class(self):
        qs = self.get_statistic()
        model = qs.model
        return model

    class Meta:
        abstract = True