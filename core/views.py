from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from pytils.translit import slugify
from django.db.models import Count, Sum, Avg, Max, Min
from collection.models import Resource
from core.models import Rating, Scale
from news.models import News, Content, Announcement


def get_filename(filename):
    ext = filename.split('.')[-1]
    name = filename.split('.')[0:-1]
    filename = "%s.%s" % (slugify(name), ext)
    return filename


def search(request):
    q = request.GET.get('q', False)
    news = News.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    announcement = Announcement.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))
    resource = Resource.objects.filter(
        Q(title__icontains=q) |
        Q(description__icontains=q) |
        Q(collection__title__icontains=q) |
        Q(collection__description__icontains=q)
    )
    objs = list(news)+list(announcement)+list(resource)
    data = {
        'objs': objs,
    }
    return render(request, 'search.html', data)


def statistic(request, obj_class, obj_id):
    from django.db.models.functions import TruncDate, ExtractDay, TruncDay

    content_type = ContentType.objects.get(model=obj_class)
    model = content_type.model_class()
    obj = model.objects.get(id=obj_id)
    qs = obj.get_statistic()
    class_model = obj.get_class()
    scale = obj.scale
    rating_type = ContentType.objects.get_for_model(model=class_model)

    return_list = []
    title_list = ['Дата']
    title_list += [q.title for q in qs]
    return_list.append(title_list)

    ratings = Rating.objects.filter(content_type=rating_type, object_id__in=qs, scale=scale)
    dates = ratings.order_by('date').values('date').distinct().values_list('date', flat=True)

    table_data = ratings.values('object_id').annotate(avg_result=Avg('result')).values('object_id', 'avg_result').order_by('-avg_result')

    for td in table_data:
        td['obj'] = qs.get(id=td['object_id'])

    if dates.count() > 0:
        start_date = dates[0]
    for d in dates:
        return_list_append = [d]
        for q in qs:
            objs = ratings.filter(object_id=q.id, date__gte=start_date, date__lte=d).order_by('object_id')
            objs = objs.values('object_id')
            objs = objs.annotate(avg_result=Avg('result'))
            avg = list(objs.values_list('avg_result', flat=True))
            if len(avg) > 0:
                avg = avg[0]
            else:
                avg = 0
            return_list_append.append(avg)
        return_list.append(return_list_append)
    data = {
        'obj': obj,
        'objs': return_list,
        'table_data': table_data
    }
    return render(request, 'resource/statistic.html', data)


def rating(request):
    if request.is_ajax() and request.POST:
        session_rating = request.session.get('rating', {})

        obj_class = request.POST.get('obj_class', None)
        obj_id = request.POST.get('obj_id', None)
        rating = request.POST.get('rating', None)
        scale_id = request.POST.get('scale_id', None)

        data_session = "%s_%s_%s" % (obj_class, obj_id, scale_id)
        content_type = ContentType.objects.get(model=obj_class)
        scale = Scale.objects.get(id=scale_id)

        if session_rating:
            if data_session in session_rating and session_rating[data_session]['result'] != rating:
                try:
                    edit_rating = Rating.objects.get(id=session_rating[data_session]['r_id'])
                    edit_rating.result = rating
                    edit_rating.save()
                    session_rating[data_session]['result'] = rating
                    request.session.modified = True
                    return HttpResponse(status=200)
                except Rating.DoesNotExist:
                    del session_rating[data_session]['r_id']
            elif data_session in session_rating and session_rating[data_session]['result'] == rating:
                return HttpResponse(status=200)
        else:
            request.session['rating'] = {}

        if obj_class and obj_id and rating and scale_id:
            new_rating = Rating(
                content_type=content_type,
                object_id=obj_id,
                result=rating,
                scale=scale
            )
            new_rating.save()

            session_rating.update({
                data_session: {
                    'result': rating,
                    'r_id': new_rating.id
                }
            })
            request.session['rating'] = session_rating

            return HttpResponse(status=200)
        return HttpResponse(status=500)