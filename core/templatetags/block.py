from django import template
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db.models import Count, Sum, Avg
from core.models import Rating

register = template.Library()

@register.filter
def do_range(val):
    return range(val)


@register.filter
def model_class(obj):
    return obj._meta.model.__name__.lower()



@register.inclusion_tag("rating.html")
def get_rating(request, obj, scale):
    session_rating = request.session.get('rating', {})
    data_session = "%s_%s_%s" % (obj._meta.model.__name__.lower(), obj.id, scale.id)
    active = False
    result = 0

    if session_rating and data_session in session_rating:
        active = True
        result = session_rating[data_session]['result']

    content_type = ContentType.objects.get_for_model(obj)
    total_result = Rating.objects.filter(
        content_type=content_type,
        object_id=obj.id,
        scale=scale
    ).aggregate(count_result=Count('result'), avg_result=Avg('result'))



    return {
        'model_class': obj._meta.model.__name__.lower(),
        'scale': scale,
        'obj': obj,
        'active': active,
        'result': int(result),
        'range': range(scale.result),
        'total_result': total_result
            }




# @register.simple_tag
# def get_rating(request, obj_class, obj_id, scale_id):



@register.filter
def nbsp_kill(obj):
    obj = obj.replace('&nbsp;', ' ').replace('&laquo;', '"').replace('&raquo;', '"').replace('&quot;', '"')
    return obj


@register.filter
def get_admin_url(obj,func):
        content_type = ContentType.objects.get_for_model(obj)
        if func=="change":
            return reverse("admin:%s_%s_change" % (
            content_type.app_label,
            content_type.model),
            args=(obj.id,))
        elif func=="add":
            return reverse("admin:%s_%s_add" % (
            content_type.app_label,
            content_type.model))
        elif func=="delete":
            return reverse("admin:%s_%s_delete" % (
            content_type.app_label,
            content_type.model),
            args=(obj.id,))
        elif func=="changelist":
            return reverse("admin:%s_%s_changelist" % (
            content_type.app_label,
            content_type.model))
        elif func=="history":
            return reverse("admin:%s_%s_history" % (
            content_type.app_label,
            content_type.model),
            args=(obj.id,))
