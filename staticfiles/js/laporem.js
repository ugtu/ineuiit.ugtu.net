function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

var coo = getCookie('individ');
var voice = getCookie('voice');
var font_size = getCookie('font_size');
var interval = getCookie('interval');
var backg = getCookie('backg');
var img_display = getCookie('img_display');

if (!font_size){
        setCookie('font_size',26,{'path':'/'})
}
if (!voice){
        setCookie('voice','m',{'path':'/'})
}
if (!img_display){
        setCookie('img_display','disabled',{'path':'/'})
}
if (!backg){
        setCookie('backg','wb',{'path':'/'})
}
if (!interval){
        setCookie('interval',1.6,{'path':'/'})
}
function deleteCookie(name) {
  setCookie(name, "", {
    expires: -1
  })
}
function getSelect() {
    return String((!!document.getSelection) ? document.getSelection() :
           (!!window.getSelection)   ? window.getSelection() :
           document.selection.createRange().text)
}

var speaker;
    if (voice=='w'){
       speaker ='omazh'
    }else{
       speaker ='zahar'
    }
var status_voice=0;


window.onload = function () {
    window.ya.speechkit.settings.apikey = '8a198424-5db7-4a30-9f9e-24bc96607085';
    var tts= new ya.speechkit.Tts();
    function speak(text,speed) {
        if (!speed){
            speed=1.1
        }
        if (status_voice==0) {
            status_voice = 1;
            if (text) {
                stro = text;
            } else {
                stro = getSelect();
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                if (window.getSelection) {
                    var selection = window.getSelection();
                    selection.removeAllRanges();
                }
                else {
                    if (document.selection.createRange) {        // Internet Explorer
                        var range = document.selection.createRange();
                        document.selection.empty();
                    }
                }
            }
            if ((stro != '' && stro)) {
                tts.speak(
                    stro,
                    {
                        speaker: speaker,
                        emotion: 'good',
                        speed: speed,
                        fast: true,
                        stopCallback: function () {
                            status_voice = 0;
                        }
                    })
            }else{
                status_voice = 0;
            }
        }
    }


    $('.say_voice').click(function () {
        speak("Приветствуем вас!!! Это технология синтеза речи. Для озвучивания текста выделите его.")
    });


    $(document).mouseup(function() {
        if (coo == 1) {
                speak();
        }
    });
    if (coo == 1) {
        if ($('.read_text').length > 0) {
            speak("Для автоматического чтения текста новости нажмите клавишу 'Ввод'...", 1)
        }
        $('.read_text').click(function () {
            var text_read = $('.page_tit').text() + $('.page_disc').text();
            speak(text_read);
        });
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('.read_text').click()
            }
        });
    }
};




function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function font_add() {
   font_size = getCookie('font_size');
   setCookie('font_size',parseInt(font_size) +1,{'path':'/'});
   $('.size_a *').css({'font-size': parseInt(font_size) +1+'px'});
}
function font_ded() {
   font_size = getCookie('font_size');
   setCookie('font_size',parseInt(font_size) -1,{'path':'/'});
   $('.size_a *').css({'font-size': parseInt(font_size) -1+'px'});
}

function set_voice(val) {
   setCookie('voice',val,{'path':'/'});
   if (val=='w'){
       speaker ='omazh'
    }else{
       speaker ='zahar'
    }
}


function interval_add() {
   interval = getCookie('interval');
   setCookie('interval',parseFloat(interval)+0.2,{'path':'/'});
   $('.size_a *').css({'line-height': parseFloat(interval)+0.2});

}
function interval_ded() {
   interval = getCookie('interval');
   setCookie('interval',parseFloat(interval)-0.2,{'path':'/'});
   $('.size_a *').css({'line-height': parseFloat(interval)-0.2});
}

function set_img(val) {
   setCookie('img_display',val,{'path':'/'});
   if (val=='disabled'){
         $('.size_a img').hide().removeClass('img_grey')
   }else if(val=='grey'){
        $('.size_a img').show().addClass('img_grey')
   }else{
       $('.size_a img').show().removeClass('img_grey')
   }
}

function set_backg(sta) {
   setCookie('backg',sta,{'path':'/'});
   if (sta=='wb'){
            $('.size_a *').css({
                'background':'none',
                'background-color': 'white',
                'color': 'black'})
        }else if(sta=='bw'){
            $('.size_a *').css({
                'background':'none',
                'background-color': 'black',
                'color': 'white'})
        }else if(sta=='by'){
            $('.size_a *').css({
                'background':'none',
                'background-color': 'darkblue',
                'color': 'yellow'})
        }
}

$(document).ready(function() {
    
    $('.rating_block').click(function () {
        event.stopPropagation();

        var base = $(this).closest('.rating__base');
        $('.rating_block', base).removeClass('active');
        $(this).parents('.rating_block').addClass('active');
        $(this).addClass('active');
        base.addClass('active');

        $.ajax({
          method: "POST",
          url: "/rating/",
          data: {
            'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
            'scale_id': base.data('scale-id'),
            'obj_id': base.data('obj-id'),
            'obj_class': base.data('obj-class'),
            'rating': $(this).data('rating')
          }
        }).done(function() {
          console.log('done')
        });
    });
    
    
    $('.individ').click(function(){
        $('.size_a *').css({'font-size': font_size+'px','line-height': interval});
        if (coo == 1){
            setCookie('individ',0,{'path':'/'})
        }else{
            setCookie('individ',1,{'path':'/'})
        }

        location.reload();
    });
    if (coo == 1) {
        $('body').addClass('size_a');
        $('._individ').show();
        $('.size_a *:not(i):not(h1):not(h2):not(h3):not(.individ)').css({'font-size': font_size+'px','line-height': interval});
        // console.log(backg);
        if (backg=='wb'){
            $('.size_a *').css({
                'background':'none',
                'background-color': 'white',
                'color': 'black'})
        }else if(backg=='bw'){
            $('.size_a *').css({
                'background':'none',
                'background-color': 'black',
                'color': 'white'})
        }else if(backg=='by'){
            $('.size_a *').css({
                'background':'none',
                'background-color': 'darkblue',
                'color': 'yellow'})
        }
        if (img_display=='disabled'){
            $('.size_a img').hide()
        }else if(img_display=='grey'){
            $('.size_a img').addClass('img_grey')
        }
    }


});



