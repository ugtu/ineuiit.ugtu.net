$(document).ready(function() {

    var autoplaySlider = $('#slider').lightSlider({
        adaptiveHeight:true,
        item:1,
        slideMargin:0,
        loop:true,
        auto:true,
        slideMove:0,
        speed:1200,
        pause:5000,
        mode: 'fade',
        addClass:'base_slider',
        controls: true
    });

    $('#slider_news').lightSlider({
        gallery: true,
        pager: true,
        item: 1,
        loop: true,
        addClass:'news_slider',
        thumbItem: 5,
        slideMargin: 0,
        currentPagerPosition: 'left',
        onSliderLoad: function (el) {
            el.lightGallery({
                selector: '#slider_news .lslide',
                exThumbImage: 'data-exthumbimage',
                thumbContHeight: 120,
            });
        }
    });

});