from django.apps import AppConfig


class LaporemCommentsConfig(AppConfig):
    name = 'laporem_comments'
