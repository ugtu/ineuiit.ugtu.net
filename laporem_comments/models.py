from django.db import models


class Comment(models.Model):
    url = models.TextField(null=True, editable=False)
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.CharField(max_length=200, blank=True)
    text = models.TextField()

    class Meta:
        ordering = ['-datetime']
