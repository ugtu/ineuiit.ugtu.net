from django import template
from laporem_comments.models import Comment

register = template.Library()


@register.inclusion_tag("comments/index.html")
def comments(request):
    url = "https://" + request.get_host() + request.get_full_path()
    cs = Comment.objects.filter(url=url).exclude(url='')
    return {'comments': cs, 'request': request}
