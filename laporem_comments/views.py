from django.http import HttpResponse
from django.shortcuts import render

from laporem_comments.models import Comment


def send(request):
    if request.POST and request.is_ajax():
        user = request.POST.get('laporemCommentesName', "").strip()
        text = request.POST.get('laporemCommentesText', "").strip()
        url = request.META.get('HTTP_REFERER', "")
        newComment = Comment(
            user=user,
            text=text,
            url=url
        )
        newComment.save()
        return render(request, 'comments/comment.html', {
            'comment': newComment,
        })


def delete(request):
    if request.POST and request.is_ajax() and request.user.is_superuser:
        c = Comment.objects.get(id=request.POST.get('id', None))
        c.delete()
        return HttpResponse(status=200)
    return HttpResponse(status=500)