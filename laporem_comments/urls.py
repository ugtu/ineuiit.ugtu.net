from django.conf.urls import include, url

import laporem_comments.views

urlpatterns = [
    url(r'^send/$', laporem_comments.views.send, name='send'),
    url(r'^delete/$', laporem_comments.views.delete, name='delete'),
]
