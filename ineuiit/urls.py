from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
import django.views.static
import news.views
import core.views
import collection.views

urlpatterns = [
    url(r'^$', news.views.home, name='home'),
    url(r'^search/$', core.views.search, name='search'),
    url(r'^news/$', news.views.news_list, name='news_list'),
    url(r'^announcements/$', news.views.announcements_list, name='announcements_list'),

    url(r'^news/(?P<slug>[\w-]+)/', news.views.news_obj, name='news_obj'),
    url(r'^announcements/(?P<slug>[\w-]+)/', news.views.announcements_obj, name='announcements_obj'),

    url(r'^rating/$', core.views.rating, name='rating'),
    url(r'^rating/statistic/(?P<obj_class>[\w-]+)_(?P<obj_id>\d+)/$', core.views.statistic, name='statistic'),

    url(r'^resource[/\w-]*/(?P<slug>[\w-]+)/$', collection.views.resource, name='resource'),
    url(r'^resource[/\w-]*/(?P<slug>[\w-]+)/(?P<slug_c>[\w-]+)$', collection.views.collection, name='collection'),

    url(r'^laporem/', include('laporem_field.urls', namespace='laporem')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^laporemComment/', include('laporem_comments.urls', namespace='laporemComments')),
    url(r'^ckeditor/', include ('ckeditor_uploader.urls')),
]


if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', django.views.static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^staticfiles/(?P<path>.*)$', django.views.static.serve, {
            'document_root': settings.STATIC_ROOT,
        }),
    ]