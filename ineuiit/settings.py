from easy_thumbnails.conf import Settings as thumbnail_settings
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY = 'pwk&l@u99@^1%3t(h_kbhe#&w_r7jag8^6=+(5b6y-7)dmsreh'
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'sitetree',
    'easy_thumbnails',
    'laporem_field',
    'ckeditor',
    'ckeditor_uploader',
    'core',
    'image_cropping',
    'solo',
    'news',
    'collection',
    'laporem_comments'
)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR,  'templates'),
        ],
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                "django.contrib.messages.context_processors.messages",
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ]
        },
    },
]
ROOT_URLCONF = 'ineuiit.urls'
WSGI_APPLICATION = 'ineuiit.wsgi.application'

LANGUAGE_CODE = 'ru'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = False
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "staticfiles"),
)
AUTH_USER_MODEL = 'core.User'
APPEND_SLASH = True

CKEDITOR_UPLOAD_PATH=('upload/')
CKEDITOR_FILENAME_GENERATOR = 'core.views.get_filename'
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'width': '100%',
    },
}



THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS
IMAGE_CROPPING_SIZE_WARNING = True
IMAGE_CROPPING_JQUERY_URL = None
THUMBNAIL_ALIASES = {
    '': {
        'small': {'size': (150, 150), 'crop': True},
        'fullmode': {'crop': "True", 'size': (1240, 0)},
        'preview': {'crop': "True", 'size': (260, 350)},
        'galleryThum': {'crop': 'True', 'size': (200, 0)},
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
   }
}
SESSION_COOKIE_HTTPONLY = True
try:
    from .local_settings import *
except:pass
