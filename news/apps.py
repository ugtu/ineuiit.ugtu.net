from django.apps import AppConfig

class YourAppConfig(AppConfig):
    name = 'news'
    verbose_name = 'Контент'