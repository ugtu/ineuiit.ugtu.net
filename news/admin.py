from django.contrib import admin
from image_cropping import ImageCroppingMixin
from .models import Events, News, Announcement, Banner


class CROP_FIELD(ImageCroppingMixin, admin.ModelAdmin):
    pass


class NewsAdmin(CROP_FIELD):
    pass


admin.site.register(News, NewsAdmin)
admin.site.register(Events, CROP_FIELD)
admin.site.register(Announcement, NewsAdmin)
admin.site.register(Banner)

