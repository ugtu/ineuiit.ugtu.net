import datetime

from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from model_utils.managers import InheritanceManager
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from django.db import models
from image_cropping import ImageRatioField
import os
from pytils.translit import slugify
from laporem_field.forms import LaporemImageField
from laporem_field.models import LaporemImage

from collection.models import Resource


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    name = filename.split('.')[0:-1]
    filename = "%s.%s" % (slugify(name), ext)
    return os.path.join('news', filename)


class ImageNews(LaporemImage):
    pass


class Events(models.Model):
    title = models.CharField(max_length=500, verbose_name="Заголовок")
    description = RichTextUploadingField(blank=True, verbose_name="Описание")
    general_image = models.ImageField(upload_to=get_file_path, blank=True, verbose_name="Главное фото", null=True)
    cropping = ImageRatioField('general_image', '265x165', verbose_name="Обрезка для превью")
    slider = LaporemImageField(null=True, blank=True, laporem_model_file=ImageNews, verbose_name="Изображения",
                               help_text="Галерея фото")
    slug = AutoSlugField(populate_from='title', max_length=1000, unique_with=['id', ])
    date = models.DateTimeField(default=datetime.datetime.now, verbose_name="Дата")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'


class Content(models.Model):
    title = models.CharField(max_length=500, verbose_name="Заголовок", blank=True)
    description = RichTextUploadingField(blank=True, verbose_name="Описание")
    slider = LaporemImageField(null=True, blank=True, laporem_model_file=ImageNews, verbose_name="Изображения",
                               help_text="Галерея фото")
    slug = AutoSlugField(populate_from='title', max_length=1000, unique_with=['id', ])
    date = models.DateTimeField(default=datetime.datetime.now, verbose_name="Дата")
    parent = models.ForeignKey(Resource, null=True, blank=True, verbose_name='Относится к ресурсу')
    objects = InheritanceManager()

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return self.title


class Banner(models.Model):
    image = models.ImageField(upload_to=get_file_path, verbose_name="Фото", null=True)
    url = models.CharField(blank=True, max_length=500)

    def __str__(self):
        return self.url

    class Meta:
        verbose_name = 'Банер'
        verbose_name_plural = 'Банеры'


class News(Content):

    def get_absolute_url(self):
        return reverse('news_obj', args=[self.slug])

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class Announcement(Content):

    def get_absolute_url(self):
        return reverse('announcements_obj', args=[self.slug])

    class Meta:
        verbose_name = 'Анонс'
        verbose_name_plural = 'Анонсы'


class Page(Content):

    def get_absolute_url(self):
        return reverse('page_obj', args=[self.slug])

    class Meta:
        verbose_name = 'Анонс'
        verbose_name_plural = 'Анонсы'