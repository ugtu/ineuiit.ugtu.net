import copy
from wsgiref.util import FileWrapper

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils.encoding import smart_str

from core.models import SiteConfiguration, Partner
from news.models import Events, News, Announcement, Banner


def pagination(objs, page):
    paginator = Paginator(objs, 12)
    try:
        objs = paginator.page(page)
    except PageNotAnInteger:
        objs = paginator.page(1)
    except EmptyPage:
        objs = paginator.page(paginator.num_pages)
    return objs


def home(request):

    # from django.utils.http import urlquote  as django_urlquote
    # from news.models import ImageNews
    #
    # fname = 'файл.jpg'
    #
    # file = ImageNews.objects.all()[:1].get().file
    # response = HttpResponse(FileWrapper(file), content_type='image/force-download')
    # response['Content-Disposition'] = 'attachment; filename=%s' % django_urlquote(fname)
    # # response['X-Sendfile'] = smart_str(file.doc.url)
    # return response


    # from PIL import Image
    # import xml.etree.ElementTree as ET
    # f = Banner.objects.first().image.file
    # with Image.open(f) as im:
    #     exif=im._getexif()
    #     xmp = im.app['APP1']
    #     xmp = xmp.decode('utf-8')
    #     xmp_start = xmp.find('<x:xmpmeta')
    #     xmp_end = xmp.find('</x:xmpmeta')
    #     xmp_str = xmp[xmp_start:xmp_end + 12]
    #     root = ET.fromstring(xmp_str)
    #     rdf = root[0]
    #     description = rdf[0]
    #     creator = description.find('{http://purl.org/dc/elements/1.1/}creator').find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Seq')
    #     for actor in creator.findall('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}li'):
    #         r=actor.text
    #         x=1

    setting = SiteConfiguration.get_solo()
    news = News.objects.order_by('-date')[:5]
    partners = Partner.objects.all()
    events = Events.objects.order_by('-date')
    announcement = Announcement.objects.order_by('-date')[:5]
    banners = Banner.objects.all()
    data = {
        'setting': setting,
        'events': events,
        'news': news,
        'announcement': announcement,
        'banners': banners,
        'partners': partners
    }
    return render(request, 'index.html', data)


def news_list(request):
    # count = News.objects.all().count()
    # for x in range(0, 50):
    #     n = News.objects.first()
    #     n.id = None
    #     n.pk = None
    #     n.save()
    # count = News.objects.all().count()
    news = News.objects.order_by('-date')
    page = request.GET.get('page')
    news = pagination(news, page)
    data = {
        'objs': news,
    }
    return render(request, 'news/news_all.html', data)


def news_obj(request, slug):
    obj = get_object_or_404(News, slug=slug)
    data = {
        'obj': obj,
    }
    return render(request, 'news/select_news.html', data)


def announcements_obj(request, slug):
    obj = get_object_or_404(Announcement, slug=slug)
    data = {
        'obj': obj,
    }
    return render(request, 'news/select_news.html', data)



def announcements_list(request):
    announcements = Announcement.objects.order_by('-date')
    page = request.GET.get('page')
    announcements = pagination(announcements, page)
    data = {
        'objs': announcements,
    }
    return render(request, 'announcements/announcements_all.html', data)